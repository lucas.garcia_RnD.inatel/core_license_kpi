import time

import pandas as pd
import ftplib
import os
import gzip
import shutil
from datetime import datetime, timedelta
import mysql.connector as mysql
import platform
import sys


def create_index(row):
    time = str(row['TIME']).replace('-', '_')
    time = time.replace(' ', '_')
    time = time.split(':')[0]
    ne_name = row['NE_NAME']
    resource_name = row['RESOURCE_ITEM']

    return f'{time}_{ne_name}_{resource_name}'


def ftp_connection(host, download_dict):
    try:
        ftp = ftplib.FTP(host)
        # if host in ['201.69.136.109', '201.69.136.110']:
        #     user = download_dict['user_1']
        #     password = download_dict['password_1']
        # else:
        user = download_dict['user_1']
        password = download_dict['password_1']

        directory = download_dict['directory']
        ftp.login(user, password)
        ftp.cwd(directory)
        files = ftp.nlst()
        file_names = []
        file_directories = []

        df_ftp = pd.DataFrame(columns=['file_name', 'file_directory'])

        df_download = pd.DataFrame(columns=['file_directory', 'file_name'])

        if 'MMLTaskResult' in files:
            print(f'- Mapping files from {host}')
            directory = f'{directory}/MMLTaskResult'
            ftp.cwd(directory)
            files = ftp.nlst()
            print('1')
        else:
            print(f'No files to download from {host}')
            return ftp, df_ftp

        for current_file in files:
            file_names.append(current_file)
            current_directory = f'{directory}/{current_file}'
            file_directories.append(current_directory)

        df_ftp['file_name'] = file_names
        df_ftp['file_directory'] = file_directories
        df_ftp['final_step'] = False
        print('2')

        df_ftp = df_ftp[df_ftp['file_name'] != '.']
        df_ftp = df_ftp[df_ftp['file_name'] != '..']

        df_ftp = df_ftp[df_ftp['file_name'].str.isdigit()]

        df_download_name = []
        df_file_name = []

        for index, row in df_ftp.iterrows():

            file_path = f"{row['file_directory']}/history"


            try:
                ftp.cwd(file_path)
            except:
                df_ftp.drop(index, inplace=True)
                print('except1')
                continue

            files = ftp.nlst()

            if any([x for x in files if 'MMLTASK_LICENSERES_PS' in x.upper()]):

                df_download_name.extend([f'{file_path}/{x}' for x in files if 'MMLTASK_LICENSERES_PS' in x.upper()])

                df_file_name.extend([x for x in files if 'MMLTASK_LICENSERES_PS' in x.upper()])

                df_ftp.loc[index, 'file_directory'] = f"{row['file_directory']}/output/MmlTaskResult_Download.txt.tar.gz"
                df_ftp.loc[index, 'file_name'] = "MmlTaskResult_Download.txt.tar.gz"
            else:
                ftp.cwd(directory)
                df_ftp.drop(index, inplace=True)

        df_download['file_directory'] = df_download_name
        df_download['file_name'] = df_file_name

        #now = datetime.now()
        #now = now.strftime('%Y%m%d_')

        last_hour_date_time = datetime.now() - timedelta(hours = 1)
        now = last_hour_date_time.strftime('%Y%m%d_%H')
        print('3')
        df_download = df_download[df_download['file_name'].str.contains(now)]
        print('4')

        print(ftp)
        for item in df_download.items():
           print(item)

        return ftp, df_download
        print('5')

    except Exception as e:
        print(e)
        print(sys.exc_info()[0])
        print(f'>>>>Error on Access {host}<<<<')
        try:
            ftp.close()
        except:
            return False, False


def download_files(download_dict):

    print('>>>>Starting files download!<<<<')

    hosts = download_dict['hosts']

    index_download = 0

    downloaded_files_df = pd.DataFrame(columns=['host', 'file_path'])

    dir_path = os.path.dirname(os.path.realpath(__file__))
    dir_path = os.path.join(dir_path, 'files')

    for host in hosts:

        try:

            ftp, df_ftp = ftp_connection(host, download_dict)

            if df_ftp.empty:
                print('>>>>Aborting files download!<<<<')
                ftp.close()
                continue

            for index, row in df_ftp.iterrows():
                print('\n===================================\n')
                file_to_download = row['file_directory']
                print(f'{host}: file_to_download = {file_to_download}')
                file_to_download_name = row['file_name']
                print(f'{host}: file_to_download_name = {file_to_download_name}')

                full_file_path = os.path.join(dir_path, 'zip')
                print(f'{host}: full_file_path = {full_file_path}')
                full_file_path = os.path.join(full_file_path, file_to_download_name)
                print(f'{host}: full_file_path_2 = {full_file_path}')
                print('\n===================================\n')
                ftp.retrbinary("RETR " + file_to_download, open(full_file_path, 'wb').write)
                print(f'Downloaded: {file_to_download_name}')
                index_download += 1
                time.sleep(1)

                new_dict = {'host': [host], 'file_path': [file_to_download]}
                insert_df = pd.DataFrame(new_dict)
                downloaded_files_df = pd.concat([downloaded_files_df, insert_df], ignore_index=True)

            ftp.close()

        except Exception as e:
            print(e)
            print(sys.exc_info()[0])
            print(f'>>>>Error on Access {host}<<<<')
            try:
                ftp.close()
            except:
                pass

    print('>>>>Finished files download!<<<<')

    return downloaded_files_df


def extract_files():
    dir_path = os.path.dirname(os.path.realpath(__file__))
    file_path = os.path.join(dir_path, 'files')
    zip_directory = os.path.join(file_path, 'zip')
    txt_directory = os.path.join(file_path, 'txt')

    zip_files = [f for f in os.listdir(zip_directory) if os.path.isfile(os.path.join(zip_directory, f))]

    print('>>>>Starting files extraction!<<<<')

    for extracting in zip_files:
        full_zip_dir = os.path.join(zip_directory, extracting)
        target_file_name = str(extracting.split('.txt')[0]) + '.txt'
        full_target_dir = os.path.join(txt_directory, target_file_name)
        with gzip.open(full_zip_dir, 'rb') as f_in:
            with open(full_target_dir, 'wb') as f_out:
                try:
                    shutil.copyfileobj(f_in, f_out)
                except:
                    pass

        os.remove(full_zip_dir)

    print('>>>>Finished files extraction!<<<<')


def parse_text_files():
    print('>>>>Starting files parse and CSV generation!<<<<')
    dir_path = os.path.dirname(os.path.realpath(__file__))
    file_path = os.path.join(dir_path, 'files')
    csv_path = os.path.join(file_path, 'csv')
    txt_path = os.path.join(file_path, 'txt')
    error_path = os.path.join(file_path, 'error_log.txt')

    files = [f for f in os.listdir(txt_path)]

    response_df = pd.DataFrame(columns=['NE_NAME', 'RESOURCE_ITEM', 'RESOURCE_NAME',
                                        'TOTAL_RESOURCE', 'USED_RESOURCE', 'USAGE',
                                        'HOUR', 'DATE', 'REF_HOUR'])

    df_list = []

    for cur_file in files:
        ref_hour = 'Unknown Hour'
        file_full_path = os.path.join(txt_path, cur_file)
        try:
            file_lines = open(file_full_path, 'r')
            lines = file_lines.readlines()
            lines = [x for x in lines if not x.strip() == '']
            file_lines.close()

            commands = []

            index_start = [i for i, x in enumerate(lines) if x.upper().strip() == 'NE NAME:']

            date = cur_file.split('.')[0]

            hour = date.split('_')[-1][:-2]
            ref_hour = f'{hour[0:2]}:00:00'
            hour = f'{hour[0:2]}:{hour[2:]}'

            date = date.split('_')[-2]
            date = f'{date[0:4]}-{date[4:6]}-{date[6:]}'

            ref_hour = date + ' ' + ref_hour

            for i, cur_index in enumerate(index_start):
                index_end = [i for i, x in enumerate(lines[cur_index:-2]) if x[0:4] == '--- ']
                if index_end:
                    index_end = index_end[0]
                else:
                    continue

                cur_lines = lines[cur_index:]
                cur_lines = cur_lines[:index_end]

                if [x for x in cur_lines if 'NUMBER OF RESULTS' in x.upper()]:
                    cur_lines = [x for x in cur_lines if not 'NUMBER OF RESULTS' in x.upper()]

                commands.append(cur_lines)

            command_lines = []

            '''aqui eu estou fazendo o esforço de comentar'''
            for cur_command in commands:
                single_result = False
                ne_name = cur_command[1].strip().split('@')[-1]
                find_row = [i for i, x in enumerate(cur_command) if 'RESOURCE ITEM' in x.upper()]
                if find_row:
                    if '=' in cur_command[find_row[0]]:
                        single_result = True
                        command_lines = cur_command[find_row[0]:]
                    else:
                        command_lines = cur_command[find_row[0] + 1:]

                cur_list = []
                # resultado vertical
                if single_result:
                    for cur_line in command_lines:
                        line_result = cur_line.split('=')[-1].strip()
                        cur_list.append(line_result)

                        cur_list.insert(0, ne_name)
                        cur_list.append(hour)
                        cur_list.append(date)
                        cur_list.append(ref_hour)
                        df_list.append(cur_list)

                # resultado horizontal
                else:
                    for cur_line in command_lines:
                        cur_list = cur_line.split('  ')
                        cur_list = [x.strip() for x in cur_list if not x.strip() == '']

                        cur_list.insert(0, ne_name)
                        cur_list.append(hour)
                        cur_list.append(date)
                        cur_list.append(ref_hour)
                        df_list.append(cur_list)

        except Exception as e:
            error_file = open(error_path, 'a')
            error_file.write(f'Error on {cur_file} at {ref_hour}:\n'
                             f'{e}\n'
                             f'\n ######################################## \n')
        # apagando arquivos de texto
        os.remove(file_full_path)

    response_df = pd.DataFrame(df_list, columns=response_df.columns)

    response_df['USED_RESOURCE'] = response_df['USED_RESOURCE'].astype('int')

    response_df['TOTAL_RESOURCE'] = response_df['TOTAL_RESOURCE'].astype('int')

    agg_dict = {'TOTAL_RESOURCE': 'max',
                'USED_RESOURCE': ['mean', 'max', 'count']}

    col_group = ['NE_NAME', 'RESOURCE_ITEM', 'RESOURCE_NAME', 'REF_HOUR']

    result_df = response_df.groupby(col_group, as_index=False).agg(agg_dict)

    end_columns = ['NE_NAME', 'RESOURCE_ITEM', 'RESOURCE_NAME', 'TIME', 'MAX_TOTAL_RESOURCE', 'AVG_USED_RESOURCE',
                   'MAX_USED_RESOURCE', 'SAMPLES']

    result_df.columns = end_columns

    result_df['AVG_USED_RESOURCE'] = result_df['AVG_USED_RESOURCE'].map('{:.2f}'.format)

    result_df['ID'] = result_df.apply(create_index, axis=1)

    result_df = result_df[['ID', 'TIME', 'NE_NAME', 'RESOURCE_ITEM', 'RESOURCE_NAME', 'MAX_TOTAL_RESOURCE',
                           'MAX_USED_RESOURCE', 'AVG_USED_RESOURCE','SAMPLES']]

    now = datetime.now()
    now = now.strftime('%Y%m%d_%H')

    csv_name = f'KPI_LIC_{now}.csv'
    csv_path = os.path.join(csv_path, csv_name)
    result_df.to_csv(csv_path, header=False, index=False)

    print('>>>>Finished CSV generation!<<<<')


def csv_upload(download_dict):
    print('>>>>Starting CSV upload!<<<<')

    dir_path = os.path.dirname(os.path.realpath(__file__))
    file_path = os.path.join(dir_path, 'files')
    csv_path = os.path.join(file_path, 'csv')

    now = datetime.now()
    now = now.strftime('%Y%m%d_%H')

    csv_name = f'KPI_LIC_{now}.csv'
    csv_path = os.path.join(csv_path, csv_name)

    cur_platform = platform.system()

    if os.path.exists(csv_path):
        upload_csv_file = csv_path.replace('\\', '/')

        db = mysql.connect(
            host="172.29.200.126",
            user=download_dict['user_sql'],
            passwd=download_dict['password_sql'],
            database='npm',
            allow_local_infile=True
        )

        cursor = db.cursor()

        if cur_platform.upper() == 'WINDOWS':
            sql = f'LOAD DATA LOCAL INFILE "{upload_csv_file}" IGNORE INTO TABLE ne_licenseres_hour\n' \
                  f'FIELDS TERMINATED BY ","  ENCLOSED BY \'"\'\n' \
                  f'LINES TERMINATED BY \'\r\n\';'
        else:
            sql = f'LOAD DATA LOCAL INFILE "{upload_csv_file}" IGNORE INTO TABLE ne_licenseres_hour\n' \
                  f'FIELDS TERMINATED BY ","  ENCLOSED BY \'"\'\n' \
                  f'LINES TERMINATED BY \'\n\';'

        cursor.execute(sql)
        db.commit()

        os.remove(csv_path)

    print('>>>>Finished CSV upload!<<<<')


def delete_ftp_files(download_dict, downloaded_files_df):
    print('>>>>Deleting FTP files!<<<<')
    if downloaded_files_df.empty:
        return 'NOK'

    hosts = downloaded_files_df['host'].unique().tolist()

    for cur_host in hosts:
        print(f'- Deleting files on {cur_host}')
        cur_df = downloaded_files_df[downloaded_files_df['host'] == cur_host]

        ftp = ftplib.FTP(cur_host)

        # if cur_host in ['201.69.136.109', '201.69.136.110']:
        #     user = download_dict['user_1']
        #     password = download_dict['password_1']
        # else:
        user = download_dict['user_1']
        password = download_dict['password_1']

        try:
            ftp.login(user, password)
        except:
            try:
                ftp.close()
            except:
                pass

            continue

        files_to_delete = cur_df['file_path'].tolist()

        for deleting in files_to_delete:
            try:
                ftp.delete(deleting)
            except:
                pass

        ftp.close()

    print('>>>>Finished files deletion!<<<<')
    return 'OK'

