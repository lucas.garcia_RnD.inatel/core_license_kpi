from utils import download_files, parse_text_files, extract_files, csv_upload, delete_ftp_files
import sys

# >>> TEST FLAG <<<

dry_run = False

# >>> CREDENTIALS <<<

download_dict = {'user_1': 'ossuser',
                 'password_1': 'Changeme_123',
                 'user_sql': 'lrgsouza',
                 'password_sql': 'luc4S2020',
                 'directory': '/export/home/sysm/ftproot',
                 'hosts': ['201.69.136.100', '201.69.136.101', '201.69.136.102', '201.69.136.103', '201.69.136.104',
                           '201.69.136.105', '201.69.136.106', '201.69.136.107', '201.69.136.108', '201.69.136.109',
                           '201.69.136.110']}

if __name__ == '__main__':
    try:
        # >>> DOWNLOADING <<<

        downloaded_files_df = download_files(download_dict)

        # >> EXTRACTING <<<
    
        extract_files()

        # >> PARSING <<<

        parse_text_files()

        if not dry_run:
            # >> UPLOAD SQL <<<

            csv_upload(download_dict)

            # >> CLEAN FTP HOSTS <<<

            status = delete_ftp_files(download_dict, downloaded_files_df)

    except Exception as e:
        print(e)
        print(sys.exc_info()[0])
